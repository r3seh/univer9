# Lectures

Учите ** и виды СрЗИ и прочее, что мы проходили

## Сертификация

![sertification](images/sertification.jpg)




## Методы и средства защиты информации

```mermaid
graph LR
o[Препятствие]
m[Управление]
h[Маскировка]
r[Регламентиция]
c[Принуждение]
g[Побуждение]
subgraph Неформальные
L[Законодательные]
O[Орагнизационные]
M[Психилогические]
end
subgraph Формальные
subgraph Технические
P[Физические]
H[Аппаратные]
end
S[Программные]
end

o --> P
m --> H
h --> H
o --> H
h --> S
m --> S
r --> S
r --> O
r --> L
c --> L
g --> M
```



## Классификация СрЗИ: 

### Во варианту исполнения:

```mermaid
graph TD
root[По варианту исполунения]
root --> Программные
root --> Аппаратные
root --> Программно-аппаратные
```

### По используемым методам

```mermaid
graph TD
root[По используемым методам]
root --> Криптографические
root --> ч[Не криптографические]
```

### По комплектности

```mermaid
graph TD
root[По комплектности]
root --> complex[Комплексная защита]
root --> personal[Персональная защита]
root --> cloud[Облачная защита]
```

### По применимости

```mermaid
graph TD
root[По применимости]
root --> Гостайна
root --> ПДн
root --> ГИС
root --> АСУТП
root --> ЮрЛица
```

### По среде использования

```mermaid
graph TD
root[По среде использования]
root --> Мобильные
root --> Виртуалзация
root --> Веб
root --> Распределенные
```



### По функционалу

* Системы управления доступом

  ```mermaid
  graph TD
  iam[Системы упревления<br>доступом _IAM_]
  iam --> user[User provisioning]
  iam --> PKI
  iam --> SSO
  ```
* Защита сети

  ```mermaid
  graph TD
  net[Защита сети]
  net --> UTM
  net --> VPN
  net --> DDoS
  ```

* Endpoint

  ```mermaid
  graph TD
  endpoint[Endpoint]
  endpoint --> Antimalware 
  endpoint --> nsd[СЗИ от НСД]
  endpoint --> OS[Защищенные ОС]
  ```

* Web

  ```mermaid
  graph TD
  web[Web]
  web --> URL-Filtres
  web --> WAF
  web --> Antimalware
  web --> filtr[Фильтрация контента]
  ```
* SVM

  ```mermaid
  graph TD
  svm[SVM]
  svm --> SIEM
  svm --> m[Управление инцидентами]
  svm --> c[Контроль соответсвия]
  svm --> s[Сканер безопасности]
  ```

* Прочее

  ```mermaid
  graph TD
  other[Другое]
  other --> DLP
  other --> Виртуализация
  other --> db[Защита СУБД]
  other --> SAST/DAST
  ```