{-# LANGUAGE ScopedTypeVariables #-}

-- MAYBE TODO: http://hackage.haskell.org/package/boxes
data Threat = Threat { threatType  :: String
                     , threatPrice :: Int
                     , threatNote  :: String
                     }

data Active = Active { activeName :: String
                     , activeC    :: Threat
                     , activeI    :: Threat
                     , activeA    :: Threat
                     , activeFreq :: Int
                     , activeProb :: Double
                     , activeCost :: Int
                     }

instance Show Threat where
    show threat = mconcat $ map (\x -> x threat ++ " | ") [threatType, show . threatPrice, threatNote]


instance Show Active where
    show active = 
        take nameLenField name ++ paddingName ++ "|   |    |   |  |" ++ 
            (mconcat $ map (\(y, x) -> "\n" ++ (take nameLenField $ cycle " ") ++ "|" ++ " " ++ y ++ " | " ++ (show $ x active) ) [("К", activeC), ("Ц", activeI), ("Д", activeA)])
        where name = activeName active
              paddingName = take (max (nameLenField - length name) 0) $ cycle " "
              nameLenField = 20


ale :: Active -> Double
ale active =  (fromIntegral $ max_threat) * (activeProb active) * (fromIntegral $ activeCost active)
    where max_threat = sum $ map (\x -> threatPrice $ x active) [activeC, activeI, activeA]


roi :: Active -> Double
roi active = (ale active - (fromIntegral $ activeCost active)) / (fromIntegral $ activeCost active)

readThreat :: IO (Threat)
readThreat = do
    type_        <- putStr "\tThreat type: " >> getLine
    price :: Int <- putStr "\tThe value of the asset: " >> readLn
    note         <- putStr "\tAny note: " >> getLine
    return Threat { threatType  = type_
                  , threatPrice = price
                  , threatNote  = note
                  }
    

add_active :: [Active] -> String -> IO [Active]
add_active acts name = do
    freq :: Int     <- putStr "Number of incidents per year: " >> readLn
    prob :: Double  <- putStr "Likelihood of successful implementation of threat: " >> readLn
    cost :: Int     <- putStr "Defense cost: " >> readLn
    threatC         <- putStrLn "Сonfidentiality" >> readThreat
    threatI         <- putStrLn "Integrety" >> readThreat
    threatA         <- putStrLn "Availability" >> readThreat
    let act = Active { activeName  = name
                     , activeC     = threatC 
                     , activeI     = threatI 
                     , activeA     = threatA 
                     , activeFreq  = freq
                     , activeProb  = prob
                     , activeCost  = cost
                     }
    return $ act : acts


showHelp = do
    putStrLn "You have following commands: "
    putStrLn "    add <Asset name>  -- add new asset"
    putStrLn "    show              -- show all added assets"
    putStrLn "    help              -- show this help"
    putStrLn "    quit              -- quit the program"


run :: [Active] -> IO ()
run acts = do
    putStr "> "
    cmd <- getLine
    case words cmd of 
      ("add":[])  -> putStrLn "Specify name" >> run acts
      ("add":ws)  -> (add_active acts $ mconcat ws) >>= run
      ("show":ws) -> putStrLn "Acitve           | Cons | T  | V | Notes |"  >> 
                     putStrLn "-----------------|------|----|---|-------|"  >>
                    (putStrLn . mconcat $ map (\x ->  show x ++ "\n" ++ "ALE: " ++ (show $ ale x) ++ "\nROI: " ++ (show $ roi x) ++ "\n\n") acts) >> run acts
      ("help":ws) -> showHelp >> run acts
      ("quit":ws) -> return ()
      otherwise   -> run acts

main = do
    run []
