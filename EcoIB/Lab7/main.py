from prettytable import PrettyTable


def add_active(actives):
    active = {
        'name': input('Enter active name: '),
        'threat': {
          'К': {
            'type': input('(FOR CONFIDENTIALITY) \n Enter type of Damage: '),
            'cost': int(input('Enter cost of damage: ')),
            'note': input('Comments: ')            
          },
          'Ц': {
            'type': input('\n(FOR INTEGRITY) \n Enter type of Damage: '),
            'cost': int(input('Enter cost of damage: ')),
            'note': input('Comments: ')
          },
          'Д': {
            'type': input('\n(FOR AVAILABILITY) \n Enter type of Damage: '),
            'cost': int(input('Enter cost of damage: ')),
            'note': input('Comments: ')
            }
        },
        'frequency': int(input('\nHow often? times in one year ')),
        'probability': float(input('How likely is this threat to occur? ')),
        'defence': input('Protective measures: '),
        'cost':int(input('Cost: ')),
    }
    ALE = 0
    ALE = (active['threat']['К']['cost'] + active['threat']['Ц']['cost'] + active['threat']['Д']['cost']) * active['frequency'] * active['probability']
    
    print(f'\nALE for this active and threat = {ALE}')
    active['threat']['ALE'] = ALE
    ROI = (ALE - active['cost'])/active['cost']
    print(f'\nROI for this active and threat = {ROI}')
    active['threat']['ROI'] = ROI
    actives += [active]

def print_act(actives):
    x = PrettyTable(["Актив","Последствия","Тип ущерба","Ценность","Примечание"])
    x.padding_width = 1
    for i in actives:
        x.add_row([i["name"],"К",i['threat']['К']['type'],i['threat']['К']['cost'],i['threat']['К']['note']])
        x.add_row(["","Ц",i['threat']['Ц']['type'],i['threat']['Ц']['cost'],i['threat']['Ц']['note']])       
        x.add_row(["","Д",i['threat']['Д']['type'],i['threat']['Д']['cost'],i['threat']['Д']['note']])
        x.add_row(["","","","",""])
        x.add_row(["Protect","Cost","ALE","ROI",""])
        x.add_row([i["defence"],i["cost"],i["threat"]["ALE"],i["threat"]["ROI"],""])
    print(x)

if __name__ == '__main__':
    actives = []
    choise = int(input('Add active? \n 1 - Yes \n 0 - No \n'))
    if choise:
        add_active(actives)
        print('\n\n')
        print_act(actives)
    else:
        exit()