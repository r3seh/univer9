# NMAP

## Задание 1.

From root:
![sudoroot](./images/sudo_nmap.png)

Without:
![without_nmap.png](./images/without_nmap.png)


## Задание2

/usr/share/nmap/nmap-services

При запуске `namp -F ` сканируются порты в файле nmap-services. Благодаря этому nmap может опередлить, что "весит" на данном порту.

Имя сервиса, номер протокла/название, open-frequency, optional comments

## Задание 3

При выполнении команды `nmap -sU` показывается информация о текущем состоянии сканирования: Сколько процентов просканировано, сколько времени идет уже сканирование и сколько осталось

```bash
root@kali ~$ nmap -sU 192.168.56.1
Nmap scan report for 192.168.56.1
Host is up (0.00044s latency).
All 1000 scanned ports on 192.168.56.1 are closed
MAC Address: 0A:00:27:00:00:00 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 1099.37 seconds
```

## Задание 4

`ёnmap-service-probes` -- бд, где храняться строки (в виде регулярных выражений) для определения типа сервиса при скаинровании. 

match <название сервиса> строка для сопастовления

## Задание 5

Проверка открытых портов и работающих на них сервисах

```bash
root@kali ~$ nmap -sV scanme.nmap.org
Starting Nmap 7.70 ( https://nmap.org ) at 2019-10-25 05:13 EDT
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.32s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 996 closed ports
PORT      STATE SERVICE    VERSION
22/tcp    open  ssh        OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
80/tcp    open  http       Apache httpd 2.4.7 ((Ubuntu))
9929/tcp  open  nping-echo Nping echo
31337/tcp open  tcpwrapped
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 11.57 seconds
```

## Задание 6

`-A` - флаг для отображение информации об ОС.
`-T` - флаг, устанавливающий режим сканирование

* paranoid (0), sneaky (1) -- Против обнаружение IDS системами 
* polite (2), -- Уменьшает количество используемых ресурсов
* normal (3) -- Режим по умолчанию
* aggressive (4) and insane (5) -- для быстрого сканированию. Не дожидается ответов от запросов на определенные порты.

```bash
root@kali ~$ nmap -A -T4 scanme.nmap.org
Starting Nmap 7.0 ( https://nmap.org ) at 2019-10-25 04:13 EDT
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.11s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f
Not shown: 996 closed ports
PORT      STATE SERVICE    VERSION
22/tcp    open  ssh        OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 ac:00:a0:1a:82:ff:cc:55:99:dc:67:2b:34:97:6b:75 (DSA)
|   2048 20:3d:2d:44:62:2a:b0:5a:9d:b5:b3:05:14:c2:a6:b2 (RSA)
|   256 96:02:bb:5e:57:54:1c:4e:45:2f:56:4c:4a:24:b2:57 (ECDSA)
|_  256 33:fa:91:0f:e0:e1:7b:1f:6d:05:a2:b0:f1:54:41:56 (ED25519)
80/tcp    open  http       Apache httpd 2.4.7 ((Ubuntu))
|_http-server-header: Apache/2.4.7 (Ubuntu)
|_http-title: Go ahead and ScanMe!
9929/tcp  open  nping-echo Nping echo
31337/tcp open  tcpwrapped
Device type: storage-misc|general purpose|WAP|media device
Running (JUST GUESSING): HP embedded (86%), Linux 2.6.X|3.X (86%), Ubiquiti AirOS 5.X (86%), Infomir embedded (85%), Netgear RAIDiator 4.X (85%)
OS CPE: cpe:/h:hp:p2000_g3 cpe:/o:linux:linux_kernel:2.6.32 cpe:/o:linux:linux_kernel:3.7 cpe:/o:ubnt:airos:5.5.9 cpe:/o:linux:linux_kernel:2.6 cpe:/h:infomir:mag-250 cpe:/o:netgear:raidiator:4.2.21 cpe:/o:linux:linux_kernel:2.6.37
Aggressive OS guesses: HP P2000 G3 NAS device (86%), Linux 2.6.32 (86%), Linux 3.7 (86%), Ubiquiti AirOS 5.5.9 (86%), Ubiquiti Pico Station WAP (AirOS 5.2.6) (86%), Linux 2.6.32 - 3.13 (85%), Linux 2.6.32 - 3.1 (85%), Infomir MAG-250 set-top box (85%), Netgear RAIDiator 4.2.21 (Linux 2.6.37) (85%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 6 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 80/tcp)
HOP RTT      ADDRESS
1   2.54 ms  192.168.43.1
2   ...
3   37.31 ms 10.172.38.65
4   51.07 ms 10.166.252.75
5   51.31 ms 10.66.143.246
6   43.69 ms scanme.nmap.org (45.33.32.156)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 31.49 seconds

```

## Задание 7

nullScan: (-sN): 
![nullScan.png](./images/nullScan.png)

finScan: (-sF): 
![finScan.png](./images/finScan.png)

xMAS( -sX):
![xmasScan.png](./images/xmasScan.png)

## Задание 8

NSE -- движок для добавление своих скриптов к nmap для различного рода задач (вплоть до исполения эксплойтов). Описывается на ЯП: Lua

```bash
root@kali ~$ nmap -p80 --script "http-headers.nse" scanme.nmap.org
Starting Nmap 7.70 ( https://nmap.org ) at 2019-10-25 04:57 EDT
Nmap scan report for scanme.nmap.org (45.33.32.156)
Host is up (0.10s latency).
Other addresses for scanme.nmap.org (not scanned): 2600:3c01::f03c:91ff:fe18:bb2f

PORT   STATE SERVICE
80/tcp open  http
| http-headers: 
|   Date: Fri, 25 Oct 2019 08:58:44 GMT
|   Server: Apache/2.4.7 (Ubuntu)
|   Accept-Ranges: bytes
|   Vary: Accept-Encoding
|   Connection: close
|   Content-Type: text/html
|   
|_  (Request type: HEAD)

Nmap done: 1 IP address (1 host up) scanned in 2.21 seconds
```