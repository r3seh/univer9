{-# LANGUAGE DeriveGeneric       #-}

import GHC.Generics (Generic)
import GHC.Conc.Sync
import Data.Random (StdRandom (StdRandom), runRVar)
import Data.Random.Extras (sample)
import System.Random (getStdGen, randomR, randomRIO, StdGen, next, setStdGen)
import Data.Graph.DGraph
import Control.Monad.State.Lazy
import Text.Printf
import Data.Graph.Types
import Data.Graph.Traversal
import Data.Graph.Visualize
import Data.Hashable
import Data.List
import Data.Ord

countLaptop :: Int
countLaptop = 10
countVulns :: Int
countVulns = 10
countContr :: Int
countContr = 10

type Name   = Int

data Contr = Contr { contr_name  :: Name
                   , contr_price :: Int } deriving (Eq, Generic, Ord, Read)
instance Hashable Contr
instance Show Contr where
   show (Contr name price) = show name ++ "(" ++ show price ++ ")"

nullContr :: Contr
nullContr = Contr { contr_name = -1
                  , contr_price = 0 }

data Laptop = Laptop { lap_name  :: Name 
                     , lap_vulns :: [Vuln] } deriving (Eq, Generic, Ord, Read)
instance Hashable Laptop
instance Show Laptop where
   show (Laptop name vulns) = "L " ++ show name ++ 
                            "\nV: " ++ (mconcat $ map (\x -> (show . vuln_name $ x) ++ " ") vulns) ++ "\n"

data Vuln = Vuln { vuln_name  :: Name
                 , vuln_contr :: [Contr] 
                 , vuln_pos   :: Double 
                 , vuln_imp   :: Int } deriving (Eq, Generic, Ord, Read)
instance Hashable Vuln
instance Show Vuln where
    show (Vuln name contr pos imp) = "V " ++ show name ++ 
                                   "\nI: " ++ show imp ++ 
                                   "\nP: " ++ (printf "%.2f" pos) ++ 
                                   "\nC: " ++ (mconcat $ map (\x -> show x ++ " ") contr) ++ "\n"

mySample :: [a0] -> Int -> StdGen -> [a0]
mySample ls count = evalState (runRVar (sample count ls) StdRandom)

arcToStr :: Show a => Arc a () -> Arc String ()
arcToStr (Arc v1 v2 _)  = (show v1) --> (show v2)

addLapsVuln :: StdGen -> [Vuln] -> [Laptop] -> [Laptop]
addLapsVuln _ _ [] = []
addLapsVuln g vlns (l:ls) = newLaptop : addLapsVuln nextG vlns ls
    where newLaptop  = l { lap_vulns = mySample vlns (count `mod` countVulns) g }
          (count, nextG) = next g

addVulnContr :: StdGen -> [Contr] -> [Vuln] -> [Vuln]
addVulnContr _ _ [] = []
addVulnContr g ctrs (l:ls) = newVuln : addVulnContr nextG ctrs ls
    where newVuln  = l { vuln_contr = mySample ctrs (count `mod` 3) g
                       , vuln_pos   = pos
                       , vuln_imp   = imp}
          (count, g1)  = next g
          (pos, g2) = randomR (0.0, 1.0) g1
          (imp, nextG) = randomR (1, 100) g2

laptopGraph :: [Vuln] -> IO (DGraph Laptop ())
laptopGraph vlns = do 
    g <- getStdGen
    let laptops = addLapsVuln g vlns [Laptop i [] | i <- [1..countLaptop]] 
    let (s, g1) = next g in setStdGen g1
    val <- randomRIO (10, countLaptop*2)
    getStdGen >>= return . fromArcsList . mySample [(i --> j) | i <- laptops, j <- laptops, i /= j] val

vulnsGraph :: [Contr] -> IO (DGraph Vuln ())
vulnsGraph cntrs = do 
    g <- getStdGen
    let vulns = addVulnContr g cntrs [Vuln i [] 0.0 0 | i <- [1..countVulns]] 
    let (s, g1) = next g in setStdGen g1
    val <- randomRIO (10, countVulns*2)
    getStdGen >>= return . fromArcsList . mySample [(i --> j) | i <- vulns, j <- vulns, i /= j] val

removeUnreachable :: DGraph Vuln () -> [Arc Laptop ()] -> [Arc Laptop ()]
removeUnreachable gv []         = []
removeUnreachable gv (arc:arcs) = (if pathExists then [arc] else []) ++ (removeUnreachable gv arcs)
    where pathExists = or [(\x -> containsArc gv x) (x --> y) | x <- lap_vulns v1, y <- lap_vulns v2]
          (v1, v2) = toPair arc

getAttackGraph :: DGraph Laptop () -> DGraph Vuln () -> DGraph Laptop ()
getAttackGraph gl gv = fromArcsList attackGraph
    where attackGraph = removeUnreachable gv ls
          ls          = toArcsList gl

graphVulnsRemoveContr :: DGraph Vuln () -> [Contr] -> DGraph Vuln ()
graphVulnsRemoveContr gv cntrs = fromArcsList [v1 --> v2 
                                    | (Arc v1 v2 _) <- toArcsList gv  
                                    , not (any (\v -> (any (\x -> x `elem` cntrs) (vuln_contr v))) [v1, v2])]

showMyGraph :: (Show a, Hashable a, Eq a) => DGraph a () -> IO ThreadId
showMyGraph graph = plotDGraph . fromArcsList . map arcToStr $ toArcsList $ graph

getContrs :: Int -> IO [Contr]
getContrs 0 = return [] 
getContrs count = do
    price <- randomRIO (1, 100)
    let contr = Contr { contr_name = count
                      , contr_price = price } in do
    nextCotrs <- getContrs (count - 1)
    return (contr : nextCotrs)

makeDecision' :: [Vuln] -> [Contr] -> Int -> [Contr]
makeDecision' _      _  0      = [] 
makeDecision' []     _  _      = [] 
makeDecision' _      [] _      = [] 
makeDecision' (v:vs) cs budget = if nullContr == chosenContr then res else chosenContr : res 
    where smartChoose  []     = nullContr
          smartChoose (c:css) = if c `elem` cs then c else smartChoose css
          cur_cs              = vuln_contr v 
          chosenContr         = if cur_cs == [] && any (\x -> x `elem` cs) cur_cs
                                then nullContr
                                else smartChoose (sortOn contr_price (vuln_contr v))
          contrLeft           = delete chosenContr cs
          res                 = makeDecision' vs contrLeft (max 0 (budget - contr_price chosenContr))

makeDecision :: [Vuln] -> [Contr] -> Int -> [Contr]
makeDecision vs = makeDecision' (sortOn (Down . vuln_pos) vs)

main = do 
    contrs <- getContrs countContr
    graphVulns <- vulnsGraph contrs
    graphLaptops <- laptopGraph (vertices graphVulns)
    -- showMyGraph graphLaptops
    showMyGraph graphVulns
    let attackGraph = getAttackGraph graphLaptops graphVulns in do
    showMyGraph $ attackGraph
    putStrLn $ mconcat (map show (dfsVertices attackGraph (vertices attackGraph !! 1)))
    let newContrs = (makeDecision (vertices graphVulns) contrs 100) in do
    putStrLn . mconcat . map (\x -> show x ++ " ") $ newContrs
    let newGraphVulns = (graphVulnsRemoveContr graphVulns newContrs) in do
    showMyGraph newGraphVulns
    showMyGraph $ getAttackGraph graphLaptops newGraphVulns