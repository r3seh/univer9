# Kali Linux



## HTTrack

Запустил и получил инфу с сайта uralctf.org

![httrack.png](./images/httrack.png)

![httrack_ls.png](./images/httrack_ls.png)

---

## Zenmap

Информация о найденном хосте:

![nmap_hostInfo.png](./images/nmap_hostInfo.png)

Построенная топология:

![nmap_topology.png](./images/nmap_topology.png)

---

## Fierce

Не полный результат вывода работы программы:

```sh
$ fierce -dns yandex.ru
DNS Servers for yandex.ru:
	ns9.z5h64q92x9.net
	ns2.yandex.ru
	ns1.yandex.ru

Trying zone transfer first...
	Testing ns9.z5h64q92x9.net
		Request timed out or transfer not allowed.
	Testing ns2.yandex.ru
		Request timed out or transfer not allowed.
	Testing ns1.yandex.ru
		Request timed out or transfer not allowed.

Unsuccessful in zone transfer (it was worth a shot)
Okay, trying the good old fashioned way... brute force

Checking for wildcard DNS...
	** Found 91520389465.yandex.ru at 213.180.204.242.
	** High probability of wildcard DNS.
Now performing 2280 test(s)...
213.180.204.90	an.yandex.ru
213.180.193.90	an.yandex.ru
93.158.134.90	an.yandex.ru
87.250.250.90	an.yandex.ru
77.88.21.90	an.yandex.ru
77.88.21.71	atlas.yandex.ru
213.180.204.188	auto.yandex.ru
213.180.204.33	balance.yandex.ru
37.140.149.196	blackberry.yandex.ru
213.180.204.90	bs.yandex.ru
77.88.21.90	bs.yandex.ru
93.158.134.90	bs.yandex.ru
87.250.250.90	bs.yandex.ru
213.180.193.90	bs.yandex.ru
93.158.134.228	business.yandex.ru
77.88.21.90	bz.yandex.ru
213.180.193.90	bz.yandex.ru
87.250.250.90	bz.yandex.ru
93.158.134.90	bz.yandex.ru
213.180.204.90	bz.yandex.ru
87.250.250.87	calendar.yandex.ru
213.180.204.33	check.yandex.ru
87.250.251.227	connect.yandex.ru
213.180.204.67	cso.yandex.ru
213.180.204.21	css.yandex.ru
77.88.21.21	css.yandex.ru
```

---

## SpiderFoot

В виртуалке